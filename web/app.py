from flask import Flask, send_from_directory, abort
import os

app = Flask(__name__)

DOCROOT = "pages/"

@app.route('/<path:filepath>')
def send_file(filepath):

    path_part = filepath.split('/')

    if len(path_part) == 1:
        if path_part[0].startswith("..") or path_part[0].startswith("~"):
            abort(403) # invok 403
        else:
            filename = path_part[0]
            abs_filepath = DOCROOT
    else:
        filename = path_part[-1]
        if filename.startswith("..") or filename.startswith("~"):
            abort(403) # invok 403
        abs_filepath = filepath.replace(filename,"")
        if abs_filepath.endswith("//"):
            abort(403) # invok 403
        else:
            abs_filepath = os.path.join(DOCROOT, abs_filepath)

    return send_from_directory(abs_filepath, filename)


@app.errorhandler(404)
def page_not_found(error):
    return send_from_directory(DOCROOT, "404.html")

@app.errorhandler(403)
def page_not_allowed(error):
    return send_from_directory(DOCROOT, "403.html")



if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
