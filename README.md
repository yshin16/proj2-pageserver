A "getting started" manual for Dockers. CIS 322, Introduction to Software Engineering, at the University of Oregon. Reference: https://docs.docker.com/engine/reference/builder/

This is a simple web server based on flask. The code was written by Professor Durairajan, and motified by the author.

Author: Yeaseul Shin
Contact Adress: yshin@uoregon.edu
